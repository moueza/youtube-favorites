package services.aquisitionStrategy;

import java.util.List;

public interface AquisitionStrategy {
	public List<String> getWatchLaterSongs();
}
